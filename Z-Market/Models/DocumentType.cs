﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Z_Market.Models
{
    public class DocumentType
    {
        [Key]
        [Display(Name = "TipoDoc")]
        public int DocumentTypeID { get; set; }

        [Display(Name = "Documento")]
        [Required(ErrorMessage ="Ingrese tipo de {0}")]
        public string Description { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }

    }
}