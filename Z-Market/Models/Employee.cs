﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Z_Market.Models
{
    //[Table("Empleados")]------Nombre de la tabla en base de datos
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }
        //[Column("Name")] -----Nombre de la columna en base de datos
        [Display(Name ="Nombre(s)")]
        [Required(ErrorMessage ="Debe ingresar {0}")]
        [StringLength(30, ErrorMessage ="El campo {0} debe tener entre {2} y {1} caracteres",MinimumLength =3)]
        public string FirstName { get; set; }

        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "Debe ingresar {0}")]
        [StringLength(30, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 3)]
        public string LastName { get; set; }

        [Display(Name = "Sueldo")]
        [Required(ErrorMessage = "Debe ingresar {0}")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Salary { get; set; }

        [Display(Name = "Porcentaje de Bono")]
        [DisplayFormat(DataFormatString = "{0:P2}", ApplyFormatInEditMode = false)]
        public float BonusPercent { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [Required(ErrorMessage = "Debe ingresar {0}")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Entrada")]
        [Required(ErrorMessage = "Debe ingresar {0}")]
        [DisplayFormat(DataFormatString = "{0:hh:mm}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Time)]
        public DateTime StartTime { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "URL")]
        [DataType(DataType.Url)]
        public string URL { get; set; }

        [Display(Name = "Tipo de Documento")]
        //Si el campo se llamara diferente para hacer la relacion con el otro modelo-tabla
        //[ForeignKey("DocumentTypeID")]  [ForeignKey("Nombre_columna/propiedad_modelo_tabla_A_relacionar")]
        public int DocumentTypeID { get; set; }

        //propiedad calculada no se crea en la BD
        [NotMapped]
        public int Age { get { return DateTime.Now.Year - DateOfBirth.Year; } }


        public virtual DocumentType DocumentType { get; set; }
    }
}