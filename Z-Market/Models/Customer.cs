﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Z_Market.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }

        [Display(Name = "Nombre(s)")]
        [Required(ErrorMessage = "Debe ingresar {0} de Cliente")]
        public string CustomerFirstName { get; set; }

        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "Debe ingresar {0} de Cliente")]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName { get { return string.Format("{0} {1}",  CustomerFirstName, LastName).ToUpper();  } set {; } }

        [Display(Name = "Telefono")]
        [Required(ErrorMessage = "Debe ingresar {0}")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Display(Name = "Dirección")]
        [Required(ErrorMessage = "Debe ingresar {0} de Cliente")]
        public string Address { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Debe ingresar {0} de Cliente")]
        [Display(Name = "Folio/Número Documento")]
        public string Document { get; set; }

        [Required(ErrorMessage = "Seleccione tipo de documento")]
        public int DocumentTypeID { get; set; }
        public virtual DocumentType DocumentType { get; set; }

        public virtual ICollection<Order> Order { get; set; }

    }
}