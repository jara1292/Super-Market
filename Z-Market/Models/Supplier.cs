﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Z_Market.Models
{
    public class Supplier
    {
        [Key]
        public int SupplierID { get; set; }

        [Display(Name ="Nombre")]
        [Required(ErrorMessage ="Debe ingresar {0} de Proveedor")]
        public string Name { get; set; }

        [Display(Name="Nombre(s) de Contacto")]
        [Required(ErrorMessage = "Debe ingresar {0} de Contacto")]
        public string ContactFirstName { get; set; }

        [Display(Name = "Apellidos de Contacto")]
        [Required(ErrorMessage = "Debe ingresar {0} de Contacto")]
        public string ContactLastName { get; set; }

        [Display(Name = "Telefono")]
        [Required(ErrorMessage = "Debe ingresar {0}")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Display(Name = "Dirección")]
        [Required(ErrorMessage = "Debe ingresar {0} de Contacto")]
        public string Address { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }


    }
}